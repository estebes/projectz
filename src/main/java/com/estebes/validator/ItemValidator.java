package com.estebes.validator;

import com.estebes.entity.Expense;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Arrays;

/**
 * Created by estebes on 11-01-2017.
 */
@Component
public class ItemValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Expense.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Expense ret = (Expense) target;
        // Type validation
        if(!Arrays.asList(Expense.Type.values()).contains(ret.getType())) {
            errors.rejectValue("type", "", "A tua mae de 4");
        }
        // Description validation
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "", "Value is empty");
    }
}
