package com.estebes.controller;

import com.estebes.entity.Client;
import com.estebes.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Created by estebes on 29-11-2016.
 */
@RestController
public class ClientController {
    /**
     *
     * @return Collection containing all the clients.
     */
    @RequestMapping(value = "/api/clients", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Client>> getAllClients() {
        ArrayList<Client> ret = new ArrayList<Client>();
        Iterable<Client> temp = clientService.getAllClients();
        for(Client client: temp) {
            ret.add(client);
        }
        return new ResponseEntity<Collection<Client>>(ret, HttpStatus.OK);
    }

    /**
     *
     * @return The client corresponding to the provided email address.
     */
    @RequestMapping(value = "/api/clients/get", params = "emailAddress", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Client> getClientByEmailAdress(@RequestParam String emailAddress) {
        Optional<Client> ret = clientService.getClientByEmailAddress(emailAddress);
        if(!ret.isPresent()) return new ResponseEntity<Client>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<Client>(ret.get(), HttpStatus.OK);
    }

    /**
     *
     * @return The client corresponding to the provided email address.
     */
    @RequestMapping(value = "/api/clients/get", params = "company", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Client>> getClientByCompany(@RequestParam String company) {
        List<Client> ret = clientService.getClientByCompany(company);
        if(ret.isEmpty()) return new ResponseEntity<Collection<Client>>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<Collection<Client>>(ret, HttpStatus.OK);
    }

    /**
     *
     * @param client The new client to be added.
     * @return
     */
    @RequestMapping(value = "/api/clients/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Client> createClient(@RequestBody Client client) {
        Optional<Client> ret = clientService.create(client);
        return new ResponseEntity<Client>(ret.get(), HttpStatus.CREATED);
    }

    /**
     * @return
     */
    @RequestMapping(value = "/api/clients/delete", method = RequestMethod.DELETE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Client> deleteClient(@PathVariable("id") long id, @RequestBody Client client) {
        clientService.remove(id);
        //if(!deleted) return new ResponseEntity<Client>(HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<Client>(HttpStatus.NO_CONTENT);
    }

    /**
     * Fields
     */
    @Autowired private ClientService clientService;
}
