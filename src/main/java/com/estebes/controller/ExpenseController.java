package com.estebes.controller;

import com.estebes.entity.Expense;
import com.estebes.service.ExpenseService;
import com.estebes.validator.ItemValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by estebes on 08-01-2017.
 */
@RestController
public class ExpenseController {
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(itemValidator);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid item type")
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public void handleHttpMessageNotReadableException(HttpMessageNotReadableException exception) {
        // NO-OP
    }

    /**
     * @return Collection containing all the expenses.
     */
    @RequestMapping(value = "/api/expenses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Expense>> getAll() {
        ArrayList<Expense> ret = new ArrayList<Expense>();
        Iterable<Expense> temp = expenseService.getAll();
        for(Expense expense: temp) {
            ret.add(expense);
        }
        return new ResponseEntity<Collection<Expense>>(ret, HttpStatus.OK);
    }

    /**
     *
     * @param expense The new expense to be added.
     * @return
     */
    @RequestMapping(value = "/api/expenses/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Expense> createItem(@Valid @RequestBody Expense expense) {
        Optional<Expense> ret = expenseService.create(expense);
        return new ResponseEntity<Expense>(ret.get(), HttpStatus.CREATED);
    }

    /**
     * Fields
     */
    @Autowired private ExpenseService expenseService;
    @Autowired private ItemValidator itemValidator;
}
