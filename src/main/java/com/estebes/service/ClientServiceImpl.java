package com.estebes.service;

import com.estebes.entity.Client;
import com.estebes.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by estebes on 01-12-2016.
 */
@Service
public class ClientServiceImpl implements ClientService {
    @Override
    public Optional<Client> getClientByEmailAddress(String emailAddress) {
        Client ret = clientRepository.findByEmailAddress(emailAddress);
        return Optional.ofNullable(ret);
    }

    @Override
    public List<Client> getClientByCompany(String company) {
        List<Client> ret = clientRepository.findByCompany(company);
        return ret;
    }

    @Override
    public Iterable<Client> getAllClients() {
        Iterable<Client> ret = clientRepository.findAll();
        return ret;
    }

    @Override
    public Optional<Client> create(Client client) {
        Client ret = clientRepository.save(client);
        return Optional.ofNullable(ret);
    }

    @Override
    public void remove(long id) {
        clientRepository.delete(id);
    }

    /**
     * Fields
     */
    @Autowired private ClientRepository clientRepository;
}
