package com.estebes.service;

import com.estebes.entity.User;
import com.estebes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by estebes on 03-12-2016.
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public Optional<User> create(User user) {
        User ret = userRepository.save(user);
        return Optional.ofNullable(ret);
    }

    /**
     * Fields
     */
    @Autowired UserRepository userRepository;
}
