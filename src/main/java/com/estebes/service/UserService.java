package com.estebes.service;

import com.estebes.entity.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Created by estebes on 03-12-2016.
 */
public interface UserService {
    @Transactional
    Optional<User> create(User user);
}
