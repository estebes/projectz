package com.estebes.service;

import com.estebes.entity.Client;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by estebes on 01-12-2016.
 */
public interface ClientService {
    Optional<Client> getClientByEmailAddress(String emailAddress);

    List<Client> getClientByCompany(String company);

    Iterable<Client> getAllClients();

    @Transactional
    Optional<Client> create(Client client);

    void remove(long id);
}
