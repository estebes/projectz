package com.estebes.service;

import com.estebes.entity.Expense;
import com.estebes.repository.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Created by estebes on 08-01-2017.
 */
@Service
public class ExpenseServiceImpl implements ExpenseService {
    @Transactional
    @Override
    public Optional<Expense> create(Expense expense) {
        Expense ret = expenseRepository.save(expense);
        return Optional.ofNullable(ret);
    }

    @Override
    public Iterable<Expense> getAll() {
        Iterable<Expense> ret = expenseRepository.findAll();
        return ret;
    }

    /**
     * Fields
     */
    @Autowired private ExpenseRepository expenseRepository;
}
