package com.estebes.service;

import com.estebes.entity.Expense;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Created by estebes on 08-01-2017.
 */
public interface ExpenseService {
    @Transactional
    Optional<Expense> create(Expense expense);

    Iterable<Expense> getAll();
}
