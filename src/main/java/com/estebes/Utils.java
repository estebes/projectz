package com.estebes;

import java.security.SecureRandom;

/**
 * Created by estebes on 03-01-2017.
 */
public class Utils {
    /**
     * Generates a random password. Useful when creating new accounts.
     * @return A random generated password
     */
    public static String passwordGenerator() {
        StringBuilder password = new StringBuilder(passwordSize);
        for(int aux = 0; aux < passwordSize; aux++) {
            password.append(allowedChars.charAt(secureRandom.nextInt(allowedChars.length())));
        }
        return password.toString();
    }

    /**
     * Fields
     */
    private static final int passwordSize = 32;
    private static final String allowedChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom secureRandom = new SecureRandom();
}
