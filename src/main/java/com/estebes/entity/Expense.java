package com.estebes.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by estebes on 08-01-2017.
 */
@Entity
@Table(name = "expenses")
public class Expense {
    public Expense() {
        // Default constructor
    }

    /**
     *
     */
    @PrePersist
    @PreUpdate
    protected void onUpdate() {
        date = new Date();
    }

    /**
     * Getters and Setters
     */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Fields
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private long id;

    @Column(name = "modified", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private Type type;

    @Column(name = "value", nullable = false)
    private double value;

    @Column(name = "description", nullable = true)
    private String description;

    /**
     * Enum containing the types of expenses that can be added.
     */
    public static enum Type {
        ALIMENTATION,
        BILLS,
        CLOTHES,
        HEALTH,
        LEISURE,
        OTHER,
        TRANSPORTATION
    }
}
