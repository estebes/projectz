package com.estebes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectZ {
	public static void main(String[] args) {
		SpringApplication.run(ProjectZ.class, args);
	}
}
