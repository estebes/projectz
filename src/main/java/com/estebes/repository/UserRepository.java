package com.estebes.repository;

import com.estebes.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by estebes on 03-12-2016.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findOneByEmailAddress(String emailAddress);

    //@Query("select user from User user where user.role like ?1")
    //List<User> findByRole(User.Role role);
}