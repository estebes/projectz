package com.estebes.repository;

import com.estebes.entity.Expense;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by estebes on 08-01-2017.
 */
public interface ExpenseRepository extends CrudRepository<Expense, Long> {
    Expense findByType(Expense.Type type);
}
