package com.estebes.repository;

import com.estebes.entity.Client;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by estebes on 01-12-2016.
 */
@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {
    @Query("select client from Client client where client.emailAddress = ?1")
    Client findByEmailAddress(String emailAddress);

    @Query("select client from Client client where client.company like ?1")
    List<Client> findByCompany(String company);
}
